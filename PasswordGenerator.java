import java.util.Random;
import java.lang.StringBuilder;
public class PasswordGenerator{
    private int length;
    public PasswordGenerator(int length){
        this.length = length;
    }
    public String generateWeakPassword(){
        Random random = new Random();
        String weak="abcdefghijklmnopqrstuvwxyz";
        StringBuilder sb = new StringBuilder(this.length);
        for(int i=0; i<this.length; i++){
           sb.append(weak.charAt(random.nextInt(weak.length()))) ;
        }
        return sb.toString();
    }
    public String generateStrongPassword(){
        Random random = new Random();
        String strong="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz123456789";
        StringBuilder sb = new StringBuilder(this.length*2);
        for(int i=0; i<this.length*2; i++){
           sb.append(strong.charAt(random.nextInt(strong.length()))) ;
        }
        return sb.toString();
    }
}