import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class PasswordGeneratorTests {

    @Test
    public void test1() {
        PasswordGenerator pg = new PasswordGenerator(10);
        assertEquals(10, pg.generateWeakPassword().length());
    }

    @Test
    public void test2() {
        PasswordGenerator pg = new PasswordGenerator(8);
        assertEquals(16, pg.generateStrongPassword().length());
    }
}
