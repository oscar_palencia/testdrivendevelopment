import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;
public class LoginTest {
    @Test
    public void validateTest(){
        User[] users = new User[1];
        users[0] = new User("Bob","acafgah",UserType.ADMIN);
        Login logintest = new Login(users);
        assertEquals(true, logintest.validate("Bob","acafgah",true));
    }
}
