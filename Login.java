public class Login {
    private User[] users;

    public Login(User[] users) {
       this.users = users;
    }

    public boolean validate(String username, String password, boolean admin) {
        for(User u : this.users) {
            
            if (admin == false) {
                if(u.getUsername() == username) {
                    if(u.getPassword() == password){
                        return true;
                    }
                }
            }

            else if (admin == true) {
                if(u.getUsername() == username) {
                    if(u.getPassword() == password) {
                        if (u.getUserType() == UserType.ADMIN) {
                        return true;
                        }
                    }
                }
            }

        }
        return false;

    }
}
